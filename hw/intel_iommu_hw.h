#ifndef INTEL_IOMMU_HW_H
#define INTEL_IOMMU_HW_H




#define	DMAR_REGS_SIZE			0x100

#define DMAR_DEVICE_ID			0x0		//TODO: FIX ME

#define DMAR_DEVICES_NUM 1

#define CIRG_GLOBAL_INVALIDATION				1
#define CIRG_DOMAIN_SELECTIVE_INVALIDATION		2
#define CIRG_DEVICE_SELECTIVE_INVALIDATION		3

#define IIRG_GLOBAL_INVALIDATION				1
#define IIRG_DOMAIN_SELECTIVE_INVALIDATION		2
#define IIRG_DOMAIN_PAGE_SELECTIVE_INVALIDATION 3

//#define DEBUG_INTEL_IOMMU //make this comment to ignore printing
#ifdef DEBUG_INTEL_IOMMU
#define pr_debug(fmt, ...)                                             \
       do {                                                            \
               fprintf( stderr, "%s : " fmt "\n",                      \
                       __func__, ##__VA_ARGS__);                       \
               fflush (stderr);                                        \
       } while (0)

#else
#define pr_debug(fmt, ...)
#endif

#define TRACE_INTEL_IOMMU //make this comment to ignore printing
#ifdef TRACE_INTEL_IOMMU
static char buffer[64][64];
static int buffer_itter = 0;
#define pr_trace(fmt, ...)                                             \
       do {                                                            \
               sprintf(&buffer[buffer_itter++][0],fmt "\n",                      \
                       ##__VA_ARGS__);	\
               if (buffer_itter == 64){\
					for (buffer_itter=0 ; buffer_itter<64; buffer_itter++) \
							fprintf( stderr,&buffer[buffer_itter][0]);                      \
					buffer_itter=0; \
					fflush (stderr); \
				}\
                                                       \
       } while (0)
#define force_pr_trace										\
		do{													\
			int i;											\
			for (i=0;i<buffer_itter;i++)					\
				fprintf( stderr,&buffer[i][0]);  \
			buffer_itter=0; 								\
			fflush (stderr); 								\
		}while(0)
#else
#define pr_trace(fmt, ...)
#endif


/* Registers definitions taken from Intel's IOMMU driver */
#define	DMAR_VER_REG		0x00  /* Arch version supported by this IOMMU */
#define DMAR_CAP_REG		0x08  /* Hardware supported capabilities */
#define DMAR_ECAP_REG		0x10 /* Extended capabilities supported */
#define DMAR_GCMD_REG		0x18 /* Global command register */
#define DMAR_GSTS_REG		0x1c /* Global status register */
#define DMAR_RTADDR_REG		0x20 /* Root entry table */
#define DMAR_CCMD_REG		0x28 /* Context command reg */
#define DMAR_FSTS_REG		0x34 /* Fault Status register */
#define DMAR_FECTL_REG		0x38 /* Fault control register */
#define DMAR_FEDATA_REG		0x3c /* Fault event interrupt data register */
#define	DMAR_FEADDR_REG		0x40 /* Fault event interrupt addr register */
#define DMAR_FEUADDR_REG	0x44 /* Upper address register */
#define DMAR_AFLOG_REG		0x58 /* Advanced Fault control */
#define DMAR_PMEN_REG		0x64 /* Enable Protected Memory Region */
#define DMAR_PLMBASE_REG	0x68 /* PMRR Low addr */
#define DMAR_PLMLIMIT_REG	0x6c /* PMRR low limit */
#define DMAR_PHMBASE_REG	0x70 /* pmrr high base addr */
#define DMAR_PHMLIMIT_REG	0x78 /* pmrr high limit */
#define DMAR_IQH_REG		0x80 /* Invalidation queue head register */
#define DMAR_IQT_REG		0x88 /* Invalidation queue tail register */
#define DMAR_IQ_SHIFT		4    /* Invalidation queue head/tail shift */
#define DMAR_IQA_REG		0x90 /* Invalidation queue addr register */
#define DMAR_ICS_REG		0x98 /* Invalidation complete status register */
#define DMAR_IRTA_REG		0xb8 /* Interrupt remapping table addr register */

#define DMAR_IVA_REG		0xc0
#define DMAR_IOTLB_REG		0xc8
#define DMAR_FRR_REG		0xd0

#define field_mask(s,e) ((1UL << (s))*((1UL << ((e)-(s)+1)) - 1))
#define clear_field(r,s,e)     *(r) = ((*r) & ~field_mask(s,e))

#define IOMMU_SUB_FIELD(r,t,f,s,e)     \
static inline void encode_ ## r ## _ ## f (uint64_t *l, uint64_t v) \
{ \
       uint64_t l_ = *((uint64_t*)l+((s)/64)); \
       clear_field(&(l_), (s)%64, (e)%64); \
       l_ |= (v<<((s)%64)) & field_mask((s)%64, (e)%64); \
       *(((uint64_t*)l)+(s)/64) = l_; \
} \
\
static inline uint64_t decode_ ## r ## _ ## f (uint64_t *l) \
{ \
       return ((*(l+(s)/64)) & (field_mask((s)%64, (e)%64))) \
               >> ((s)%64); \
}


#define IOMMU_REG_FIELD(r,f,s,e)       IOMMU_SUB_FIELD(r,reg,f,s,e)
#define IOMMU_IQ_FIELD(r,f,s,e)                IOMMU_SUB_FIELD(r,iq,f,s,e)
#define IOMMU_PTE_FIELD(r,f,s,e)       IOMMU_SUB_FIELD(r,pt,f,s,e)
#define IOMMU_RE_FIELD(r,f,s,e)                IOMMU_SUB_FIELD(r,pt,f,s,e)
#define IOMMU_CE_FIELD(r,f,s,e)                IOMMU_SUB_FIELD(r,pt,f,s,e)

IOMMU_REG_FIELD(iqa, qs, 0, 2)
IOMMU_REG_FIELD(iqa, iqa, 12, 63)
IOMMU_REG_FIELD(iqh, qh, 4, 18)
IOMMU_REG_FIELD(iqt, qt, 4, 18)
IOMMU_REG_FIELD(ver, min, 0, 3)
IOMMU_REG_FIELD(cap, cm, 7, 7)
IOMMU_REG_FIELD(cap, sagaw, 8, 12)
IOMMU_REG_FIELD(cap, mgaw, 16, 21)
IOMMU_REG_FIELD(cap, psi, 39, 39)
IOMMU_REG_FIELD(cap, nfr, 40, 47)
IOMMU_REG_FIELD(cap, mamv, 48, 53)
IOMMU_REG_FIELD(cap, fro, 24, 33)
IOMMU_REG_FIELD(ecap, c, 0, 0)
IOMMU_REG_FIELD(ecap, qi, 1, 1)
IOMMU_REG_FIELD(ecap, ch, 5, 5)
IOMMU_REG_FIELD(ecap, pt, 6, 6)
IOMMU_REG_FIELD(ecap, iro, 8, 17)
IOMMU_REG_FIELD(ccmd, caig, 59, 60)
IOMMU_REG_FIELD(ccmd, cirg, 61, 62)
IOMMU_REG_FIELD(ccmd, icc, 63, 63)
IOMMU_REG_FIELD(pmen, prs, 0, 0)
IOMMU_REG_FIELD(pmen, epm, 31, 31)
IOMMU_REG_FIELD(gcmd, cfi, 23, 23)
IOMMU_REG_FIELD(gcmd, sirtp, 24, 24)
IOMMU_REG_FIELD(gcmd, ire, 25, 25)
IOMMU_REG_FIELD(gcmd, qie, 26, 26)
IOMMU_REG_FIELD(gcmd, wbf, 27, 27)
IOMMU_REG_FIELD(gcmd, eafl, 28, 28)
IOMMU_REG_FIELD(gcmd, sfl, 29, 29)
IOMMU_REG_FIELD(gcmd, srtp, 30, 30)
IOMMU_REG_FIELD(gcmd, te, 31, 31)
IOMMU_REG_FIELD(gsts, cfis, 23, 23)
IOMMU_REG_FIELD(gsts, irtps, 24, 24)
IOMMU_REG_FIELD(gsts, ires, 25, 25)
IOMMU_REG_FIELD(gsts, qies, 26, 26)
IOMMU_REG_FIELD(gsts, wbfs, 27, 27)
IOMMU_REG_FIELD(gsts, afls, 28, 28)
IOMMU_REG_FIELD(gsts, fls, 29, 29)
IOMMU_REG_FIELD(gsts, rtps, 30, 30)
IOMMU_REG_FIELD(gsts, tes, 31, 31)
IOMMU_REG_FIELD(iotlb, iaig, 57, 58)
IOMMU_REG_FIELD(iotlb, iirg, 60, 61)
IOMMU_REG_FIELD(iotlb, ivt, 63, 63)
IOMMU_REG_FIELD(iva, am, 0, 5)
IOMMU_REG_FIELD(iva, ih, 6, 6)
IOMMU_REG_FIELD(iva, addr, 12, 63)
IOMMU_REG_FIELD(rtaddr, rta, 12, 63)

enum { MGAW = 48};
enum { FRCD_REG_NUM = 4 };
enum { DMAR_REGS_PAGE = 0x1000, DMAR_REGS_PAGE_SHIFT = 12, LEVEL_STRIDE=9,
       VTD_PAGE_SHIFT=12} ;
enum { DEVFN_ENTRIES_NUM = 256 };
enum { IOMMU_WRITE = 2, IOMMU_READ = 1};

#define VTD_PAGE_SIZE (1<<VTD_PAGE_SHIFT)

/* Mapping related structures */

enum { TRASNLATE_UNTRANSLATED = 0, TRANSLATE_ALL = 1, TRANSLATE_PASS_THROUGH =
2, TRANSLATE_RESERVED = 3};

IOMMU_PTE_FIELD(pte, r, 0, 0)
IOMMU_PTE_FIELD(pte, w, 1, 1)
IOMMU_PTE_FIELD(pte, sp, 7, 7)
IOMMU_PTE_FIELD(pte, snp, 11, 11)
IOMMU_PTE_FIELD(pte, addr, 12, 51)
IOMMU_PTE_FIELD(pte, tm, 62, 62)

IOMMU_CE_FIELD(ce, p, 0, 0)
IOMMU_CE_FIELD(ce, t, 2, 3)
IOMMU_CE_FIELD(ce, asr, 12, 63)
IOMMU_CE_FIELD(ce, aw, 64, 66)

IOMMU_RE_FIELD(re, p, 0, 0)
IOMMU_RE_FIELD(re, ctp, 12, 63)

IOMMU_IQ_FIELD(iq_desc, id, 0, 3)
IOMMU_IQ_FIELD(iq_desc, iflag, 4, 4)
IOMMU_IQ_FIELD(iq_desc, sw, 5, 5)
IOMMU_IQ_FIELD(iq_desc, wait_stat_addr, 66, 127)
IOMMU_IQ_FIELD(iq_desc, wait_stat_data, 32, 63)

enum {SPS_21_BIT = 1, SPS_30_BIT = 2, SPS_39_BIT = 4, SPS_48_BIT = 8}; /*
sagaw (Adjusted Guest Address Width) values */
enum {AGAW_30_BIT = 1, AGAW_39_BIT = 2, AGAW_48_BIT = 4, AGAW_57_BIT = 8,
AGAW_64_BIT = 16}; /* agaw values */

/* Invalidation queue descriptors */
enum { IQ_CONTEXT_INVD_DESC_ID = 1,
       IQ_IOTLB_INVD_DESC_ID = 2,
       IQ_DEV_IOTLB_INVD_DESC_ID = 3,
       IQ_INT_CACHE_INV_DESC_ID = 4,
       IQ_INVD_WAIT_DESC_ID = 5 };


#endif //INTEL_IOMMU_HW_H
