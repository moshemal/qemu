#ifndef IOMMU_H
#define IOMMU_H

#include "qemu-common.h"
#include "dma.h"
#include "sysbus.h"

typedef struct IOMmuBusDevice IOMmuBusDevice;
typedef struct DMAMemoryMap DMAMemoryMap;
typedef enum IommuErrorCode IommuErrorCode;

typedef IommuErrorCode IOMMUTranslateFunc(IOMmuBusDevice *iommu,
							 PCIDevice *dev,
                             dma_addr_t addr,
                             dma_addr_t *paddr,
                             dma_addr_t *len,
                             int is_write);

typedef void DMAInvalidateMapFunc(void *);

enum IommuErrorCode{
	IOMMU_SUCCESS			= 0,
	IOMMU_PASS_THROUGH		= 1,
	IOMMU_NO_TRANSLATION	= -1,
	IOMMU_IOMMU_NOT_EXIST	= -2,
	IOMMU_NO_PERMISSION		= -3,
};

/*
 * first member in the specific iommu should be:
 * type: IOMmuBusDevice
 * name: iobusdev
 */
struct IOMmuBusDevice {
	SysBusDevice busdev;
	IOMMUTranslateFunc *translate;
    QLIST_HEAD(memory_maps, DMAMemoryMap) memory_maps;
};

/* Macros to compensate for lack of type inheritance in C.  */
#define SYSBUS_TO_IOMMU(type,dev) DO_UPCAST(type,iobusdev.busdev,dev)
#define DEV_STATE_TO_IOMMU(type,dev) DO_UPCAST(type, iobusdev.busdev.qdev, dev)

struct DMAMemoryMap {
    dma_addr_t              addr;
    dma_addr_t              len;
    target_phys_addr_t      paddr;
    DMAInvalidateMapFunc    *invalidate;
    void                    *invalidate_opaque;

    QLIST_ENTRY(DMAMemoryMap) list;
};


#endif /*IOMMU_H*/
