/*
 * QEMU INTEL IOMMU emulation
 * Copyright (c) 2012 Moshe Malka
 * moshemal at cs.technion.ac.il
 *
 */
#include <stdint.h>
#include <stdio.h>

#include "intel_iommu_hw.h"

#include "module.h"
#include "sysbus.h"
#include "dma.h"
#include "pci.h"
#include "iommu.h"


typedef struct IntelIOMMUState {
	IOMmuBusDevice iobusdev;
	uint8_t mmio_index;
	uint8_t invalidation_queue;
	uint8_t enabled;
	uint8_t dmar_regs[DMAR_DEVICES_NUM][DMAR_REGS_SIZE];
}IntelIOMMUState;


static inline uint8_t get_devfn(uint32_t sid)
{
       return (uint8_t)(sid & 0xFF);
}

static inline uint8_t get_busnr(uint32_t sid)
{
       return (uint8_t)(sid>>8)&0xFF;
}

static inline void read_reg32(uint8_t* dmar_regs, uint32_t reg,
                             uint64_t* val)
{
       *val = le32_to_cpu(*((uint32_t*)(dmar_regs+reg)));
}

static inline void read_reg64(uint8_t* dmar_regs, uint32_t reg,
                             uint64_t* val)
{
       *val = le64_to_cpu(*((uint64_t*)(dmar_regs+reg)));
}

static inline void write_reg32(uint8_t* dmar_regs, uint32_t reg,
                              uint32_t val)
{
       *(uint32_t*)(dmar_regs+reg) = cpu_to_le32(val);
}

static inline void write_reg64(uint8_t* dmar_regs, uint32_t reg,
                              uint64_t val)
{
	//pr_debug("writing 0x%lx 0x%lx to 0x%lx  and cpu_to_le=0x%lx",(unsigned int)val, *(unsigned int*)((((uint8_t*)&val)+4)),(unsigned int)reg,cpu_to_le64(val));
	*(uint64_t*)(dmar_regs+reg) = cpu_to_le64(val);
}

static inline void convert_le128_to_cpu(uint64_t u128[2])
{
       *u128 = le64_to_cpu(*u128);
       *(u128+1) = le64_to_cpu(*(u128+1));
}


/* Translation related functions */
static inline int aw_to_levels(int aw) {
       return aw+2;
}


static inline int level_shift(int level) {
       return VTD_PAGE_SHIFT+(level*LEVEL_STRIDE);
}

static inline uint64_t level_size(int level) {
       return (1ULL<<level_shift(level));
}

static inline uint64_t level_offset(uint64_t addr, int level) {
       return (addr >> level_shift(level))&((1ULL<<LEVEL_STRIDE)-1);
}

static inline uint64_t level_mask(int level) {
       return (((uint64_t)1)<<level_shift(level))-1;
}


/* Main translation function */
static
int intel_iommu_phy_addr_translate_hndl(uint8_t* dmar_regs, PCIDevice *dev,
		dma_addr_t addr, dma_addr_t *paddr, dma_addr_t* len, uint8_t* access_perm)
{
      //todo: change error codes;
	   uint64_t pte;
       int level;
       //uint8_t* dmar_regs = (iommu_state.dmar_status[0].dmar_regs);
       uint64_t temp, addr_offset, gsts, rtaddr, pte_addr;
       uint8_t devfn = get_devfn(dev->devfn);
       uint8_t busnr = get_busnr(dev->devfn);
       uint64_t re[2], ce[2];
       read_reg32(dmar_regs, DMAR_GSTS_REG, &gsts);
       if (!len)
    	   len = &temp;
       *access_perm = IOMMU_READ | IOMMU_WRITE;

       *paddr = addr;
       //*len = 0;
       /* Reading the root entry */
       read_reg64(dmar_regs, DMAR_RTADDR_REG, &rtaddr);

       cpu_physical_memory_rw(rtaddr+(busnr*sizeof(re)), (uint8_t *)re,sizeof(re), 0);
       convert_le128_to_cpu(re);

       if (!decode_gsts_tes(&gsts)/*dmar_regs->gsts.tes*/) {
               return IOMMU_NO_TRANSLATION;/* Translation is disabled */
       }

       if (!decode_gsts_rtps(&gsts)/*dmar_regs->gsts.rtps*/) {
               *access_perm = 0;
               return -1;
       }
       if (!decode_re_p(re)) {
               *access_perm = 0;
               return -1;
       }

       /* Reading the relevant context entry */

       cpu_physical_memory_rw(((decode_re_ctp(re)<<VTD_PAGE_SHIFT)+devfn*sizeof(ce)),
                       (uint8_t *)ce, sizeof(ce), 0);
       convert_le128_to_cpu(ce);

       pr_debug("devfn=%x  busnr=%x",devfn,busnr);
       if (!decode_ce_p(ce)) {
               *access_perm = 0;
               return -1;
       }

       if (decode_ce_t(ce) == TRANSLATE_RESERVED) {
               *access_perm = 0;
               return -1;
       }
       if (decode_ce_t(ce) == TRANSLATE_PASS_THROUGH) {
               return IOMMU_PASS_THROUGH;
       }

       /* Analyzing the levels number from the context */
       level = aw_to_levels(decode_ce_aw(ce));

       pte_addr = decode_ce_asr(ce) << DMAR_REGS_PAGE_SHIFT;

       while (level>0 && *access_perm != 0) {
               pte_addr += level_offset(addr, level-1)*sizeof(pte);
               cpu_physical_memory_rw(pte_addr,
                               (uint8_t *)&pte, sizeof(pte), 0);
               pte = le64_to_cpu(pte);
               pte_addr = decode_pte_addr(&pte);
               pte_addr <<= VTD_PAGE_SHIFT;
               if (!decode_pte_r(&pte))
                       (*access_perm) &= (~IOMMU_READ);
               if (!decode_pte_w(&pte))
                      (*access_perm) &= (~IOMMU_WRITE);
               if (decode_pte_sp(&pte))
                       break;          /* Super page */
               level--;
       }
       addr_offset = addr&level_mask(level);
       *len = level_size(level) - addr_offset;
       /* shift left also if super-page */
       *paddr = (pte_addr<<(LEVEL_STRIDE*level))+(addr&level_mask(level));
       return IOMMU_SUCCESS;
}

static IommuErrorCode
intel_iommu_phy_addr_translate(IOMmuBusDevice *iommu, PCIDevice *dev,
								dma_addr_t addr, dma_addr_t *paddr,
								dma_addr_t *len,int is_write)
{
//todo: fix this function
	IntelIOMMUState* iommu_state = DO_UPCAST(IntelIOMMUState,iobusdev,iommu);
	uint8_t access_perm;
	uint16_t req_access_perm = is_write ? IOMMU_WRITE : IOMMU_READ;
	IommuErrorCode err = 0;

	err = intel_iommu_phy_addr_translate_hndl(&iommu_state->dmar_regs[0][0],
			dev, addr,paddr,len,&access_perm);

	if ((req_access_perm & access_perm)==0)
		   err = IOMMU_NO_PERMISSION;

	char* access_strings[]={"read","write"};

	pr_trace("%s %x %lx %lx",access_strings[is_write],dev->devfn,addr,*paddr);
	//pr_debug("dev name: %s va: %x  pa: %x  errCode=%d  len=%d access=%s devfn=%x",dev->name,(unsigned int)addr,(unsigned int)*paddr,err,(int)*len,access_strings[is_write],dev->devfn);

	return err;
}
static
void printregs(uint8_t* arr)
{
	pr_debug("regs value:");
	int i;
	uint32_t reg;
	for (i=0; i<DMAR_REGS_SIZE; i+=4){
		reg = *(uint32_t*)&arr[i];
		printf("0x%x:  0x%x\n",(unsigned int)i,(unsigned int)reg);
	}
}
static
void iommu_reset_regs(IntelIOMMUState *s) {

	uint64_t ecap=0, cap=0,  ver=0;
	uint8_t *dmar;
	int i;

	for (i=0; i<DMAR_DEVICES_NUM; ++i){
		dmar = &s->dmar_regs[i][0];
		memset(dmar, 0, sizeof(*dmar));
		encode_ver_min(&ver, 1);
		write_reg32(dmar, DMAR_VER_REG, ver);
		encode_cap_mgaw(&cap, MGAW-1);
		encode_cap_sagaw(&cap, AGAW_30_BIT | AGAW_39_BIT | AGAW_48_BIT);
		encode_cap_nfr(&cap, FRCD_REG_NUM);
		encode_cap_psi(&cap, 1); /* enabling page selective invalidation */
		encode_cap_mamv(&cap, 63); /* setting mamv to its maximal value */
		encode_cap_cm(&cap, 1); /* caching mode is disabled to support Linux */
		encode_cap_fro(&cap, DMAR_FRR_REG / 16);
		write_reg64(dmar, DMAR_CAP_REG, cap);
		encode_ecap_pt(&ecap, 1);
		encode_ecap_ch(&ecap, 1);
		encode_ecap_c(&ecap, 1);
		encode_ecap_iro(&ecap, DMAR_IVA_REG / 16);
		encode_ecap_qi(&ecap, s->invalidation_queue);
		write_reg64(dmar, DMAR_ECAP_REG, ecap);
	}
}

static
void intel_iommu_reset(DeviceState *d)
{
	pr_trace("***intel_iommu_reset**was**called***");
	force_pr_trace;
	printf("********intel_iommu_reset was called forced printing***********\n");
	IntelIOMMUState *s = DEV_STATE_TO_IOMMU(IntelIOMMUState,d);
    s->enabled = 1;
    s->invalidation_queue = 0;
    iommu_reset_regs(s);
}



static
void intel_iommu_iq_wait(uint64_t desc[2])
{
       if (decode_iq_desc_iflag(desc)) {
               pr_debug("interrupts are not supported");
               return;
       }
       if (decode_iq_desc_sw((desc))) {
               uint32_t data = decode_iq_desc_wait_stat_data(desc);
               uint64_t addr =
                       decode_iq_desc_wait_stat_addr(desc);
               cpu_physical_memory_rw(addr << 2,
                       (uint8_t *)&data, sizeof(data), 1);
       }
}


static
int iommu_process_iq_desc (uint64_t desc[2])
{
       int r = 0;
       uint32_t id = decode_iq_desc_id(desc);
       switch (id)
       {
       case IQ_CONTEXT_INVD_DESC_ID: {
               break;
               }
       case IQ_IOTLB_INVD_DESC_ID: {
               break;
               }
       case IQ_INVD_WAIT_DESC_ID: {
               intel_iommu_iq_wait(desc);
               break;
               }
       case IQ_DEV_IOTLB_INVD_DESC_ID: {
               pr_debug("IQ_DEV_IOTLB_INVD_DESC_ID is not implemented");
               break;
               }
       case IQ_INT_CACHE_INV_DESC_ID: {
               pr_debug("IQ_INT_CACHE_INV_DESC_ID is not implemented");
               break;
               }
       default:
               r = -1;
               pr_debug("invalid descriptor id %x", id);
               break;
       }
       return r;
}


static
int intel_iommu_process_iq(uint8_t* dmar_regs)
{
       uint64_t iqa, iqt, iqh;
       uint64_t qt, qh;
       size_t len, desc_size;
       target_phys_addr_t map_len, qaddr;
       uint64 desc[2];

       read_reg64(dmar_regs, DMAR_IQA_REG, &iqa);
       read_reg64(dmar_regs, DMAR_IQT_REG, &iqt);
       read_reg64(dmar_regs, DMAR_IQH_REG, &iqh);
       len = 1 << (8 + decode_iqa_qs(&iqa));
       desc_size = sizeof(desc);
       map_len = len * desc_size;
       qaddr = decode_iqa_iqa(&iqa) << VTD_PAGE_SHIFT;
       qh = decode_iqh_qh(&iqh);
       qt = decode_iqt_qt(&iqt);

       if (map_len < len * desc_size) {
               pr_debug("could not map entire invalidation queue");
               exit(1);
       }

       while (qh != qt) {
               cpu_physical_memory_rw(qaddr + qh*desc_size,
                       (uint8_t *)&desc, sizeof(desc), 0);
               convert_le128_to_cpu(desc);
               iommu_process_iq_desc(desc);
               qh = (qh+1)%len;
       }
       encode_iqh_qh(&iqh, qh);
       write_reg64(dmar_regs, DMAR_IQH_REG, iqh);
       return 0;
}

static void
intel_iommu_write_status_update(IntelIOMMUState *s, unsigned int offset,
								unsigned int dmar_idx)
{
	//int offset = addr & 0xFFF & ~3;
	//uint8_t* dmar_regs = dmar_status->dmar_regs;

	switch (offset){
		case DMAR_GCMD_REG:{
			   uint64_t gsts;
			   uint64_t gcmd;
			   read_reg32(&s->dmar_regs[dmar_idx][0], DMAR_GCMD_REG, &gcmd);
			   read_reg32(&s->dmar_regs[dmar_idx][0], DMAR_GSTS_REG, &gsts);
			   encode_gsts_fls(&gsts, decode_gcmd_sfl(&gcmd));
			   if (decode_gcmd_srtp(&gcmd))
					   encode_gsts_rtps(&gsts, 1);
			   encode_gsts_wbfs(&gsts, decode_gcmd_wbf(&gcmd));
			   encode_gsts_tes(&gsts, decode_gcmd_te(&gcmd));
			   encode_gcmd_srtp(&gcmd, 0);
			   if (s->invalidation_queue &&
				   decode_gsts_qies(&gsts) == 0 &&
				   decode_gcmd_qie(&gcmd) == 1) {
					   encode_gsts_qies(&gsts, 1);
			   }
			   write_reg32(&s->dmar_regs[dmar_idx][0], DMAR_GSTS_REG, gsts);
			   write_reg32(&s->dmar_regs[dmar_idx][0], DMAR_GCMD_REG, gcmd);
			   break;
			   }
		case DMAR_CCMD_REG:
		case DMAR_CCMD_REG+4: {
			   uint64_t ccmd;
			   read_reg64(&s->dmar_regs[dmar_idx][0], DMAR_CCMD_REG, &ccmd);
			   encode_ccmd_caig(&ccmd, decode_ccmd_cirg(&ccmd));
			   encode_ccmd_icc(&ccmd, 0);
			   write_reg64(&s->dmar_regs[dmar_idx][0], DMAR_CCMD_REG, ccmd);
			   break;
			   }
		case DMAR_PMEN_REG: {
			   uint64_t pmen;
			   read_reg32(&s->dmar_regs[dmar_idx][0], DMAR_PMEN_REG, &pmen);
			   encode_pmen_prs(&pmen, decode_pmen_epm(&pmen));
			   write_reg32(&s->dmar_regs[dmar_idx][0], DMAR_PMEN_REG, pmen);
			   break;
			   }
		case DMAR_IOTLB_REG:
		case DMAR_IOTLB_REG+4:
		case DMAR_IVA_REG:
		case DMAR_IVA_REG+4: {
			   uint64_t iotlb;
			   uint64_t iva;
			   read_reg64(&s->dmar_regs[dmar_idx][0], DMAR_IOTLB_REG, &iotlb);
			   read_reg64(&s->dmar_regs[dmar_idx][0], DMAR_IVA_REG, &iva);
			   if (decode_iotlb_ivt(&iotlb) == 1) {
					   encode_iva_addr(&iva, 0);
					   encode_iva_am(&iva, 0);
					   encode_iva_ih(&iva, 0);
					   encode_iotlb_iaig(&iotlb, decode_iotlb_iirg(&iotlb));
					   encode_iotlb_ivt(&iotlb, 0);
					   write_reg64(&s->dmar_regs[dmar_idx][0], DMAR_IOTLB_REG, iotlb);
					   write_reg64(&s->dmar_regs[dmar_idx][0], DMAR_IVA_REG, iva);
			   }
			   break;
			   }
		case DMAR_IQT_REG:
		case DMAR_IQT_REG+4: {
			   uint64_t gsts;
			   read_reg32(&s->dmar_regs[dmar_idx][0], DMAR_GSTS_REG, &gsts);
			   if (s->invalidation_queue &&
					   decode_gsts_qies(&gsts) == 1)
					   intel_iommu_process_iq(&s->dmar_regs[dmar_idx][0]);
			   break;
			   }
		default: break;

	}
}

/*
 *
 * call back functions
 * Software is expected to access 32-bit registers as aligned doublewords. For
 * example, to modify a field (e.g., bit or byte) in a 32-bit register, the
 * entire doubleword is read, the appropriate field(s) are modified, and the
 * entire doubleword is written back.
 */

static void
intel_iommu_writel(void *opaque, target_phys_addr_t addr, uint32_t val)
{
	IntelIOMMUState *s = opaque;
	unsigned int offset = (addr & 0xFFF);
	unsigned int dmar_idx = (addr & ~(DMAR_REGS_PAGE-1)) >> DMAR_REGS_PAGE_SHIFT;

	uint32_t* reg;

	if (dmar_idx >= DMAR_DEVICES_NUM)
		   return;


	if (offset > DMAR_REGS_SIZE-4)
		   return; /* outside the registers area - reserved */
	reg = (uint32_t*)&s->dmar_regs[dmar_idx][offset];
	pr_trace("reg write %x to address %lx",val,addr);
	*reg = val;
	intel_iommu_write_status_update(s, offset,dmar_idx);
	//pr_debug("value of register after write status update= 0x%x",(uint32_t)*reg);
}

static void
intel_iommu_writew(void *opaque, target_phys_addr_t addr, uint32_t val)
{
    // emulate hw without byte enables: no RMW
    intel_iommu_writel(opaque, addr & ~3,
                      (val & 0xffff) << (8*(addr & 3)));
}

static void
intel_iommu_writeb(void *opaque, target_phys_addr_t addr, uint32_t val)
{
    // emulate hw without byte enables: no RMW
    intel_iommu_writel(opaque, addr & ~3,
                      (val & 0xff) << (8*(addr & 3)));
}


static uint32_t
intel_iommu_readl(void *opaque, target_phys_addr_t addr)
{
	IntelIOMMUState *s = opaque;
	unsigned int offset = (addr & 0xfff);
	int idx = 0; //Change it for multiple DMAR units

	uint32_t ret = 0;
	/* not outside the boundary of the registers */
	if (! (offset > DMAR_REGS_SIZE-4)) {
		ret = *(uint32_t*)(&s->dmar_regs[idx][offset]);
	}

	//pr_debug("reading addr = 0x%x  value: 0x%x",(uint32_t)addr,(uint32_t)ret );

	return ret;
}

static uint32_t
intel_iommu_readb(void *opaque, target_phys_addr_t addr)
{
    return ((intel_iommu_readl(opaque, addr & ~3)) >>
            (8 * (addr & 3))) & 0xff;
}

static uint32_t
intel_iommu_readw(void *opaque, target_phys_addr_t addr)
{
    return ((intel_iommu_readl(opaque, addr & ~3)) >>
            (8 * (addr & 3))) & 0xffff;
}


static CPUReadMemoryFunc * const intel_iommu_mem_read[3] = {
	intel_iommu_readb,
	intel_iommu_readw,
	intel_iommu_readl
};

static CPUWriteMemoryFunc * const intel_iommu_mem_write[3] = {
	intel_iommu_writeb,
	intel_iommu_writew,
	intel_iommu_writel
};

static int intel_iommu_init1(SysBusDevice *dev)
{
	IntelIOMMUState *s = SYSBUS_TO_IOMMU(IntelIOMMUState,dev);
    int io_memory;

    io_memory = cpu_register_io_memory(intel_iommu_mem_read,
    								   intel_iommu_mem_write, s,
                                       DEVICE_NATIVE_ENDIAN);

    sysbus_init_mmio(dev, DMAR_REGS_SIZE * sizeof(uint8_t), io_memory);
    s->iobusdev.translate = intel_iommu_phy_addr_translate;

    return 0;
}

static int intel_iommu_exit(DeviceState *dev)
{
	printf("********exit was called***********\n");
	return 0;
}

static const VMStateDescription vmstate_intel_iommu = {
    .name ="intel-iommu",
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields      = (VMStateField []) {
    	VMSTATE_UINT8(mmio_index,IntelIOMMUState),
    	VMSTATE_UINT8(invalidation_queue,IntelIOMMUState),
    	VMSTATE_UINT8(enabled,IntelIOMMUState),
    	//VMSTATE_UINT8_ARRAY(dmar_regs, IntelIOMMUState, DMAR_DEVICES_NUM * DMAR_REGS_SIZE),
    	VMSTATE_END_OF_LIST()
    }
};

static SysBusDeviceInfo intel_iommu_info = {
    .init = intel_iommu_init1,
    .qdev.name = "intel-iommu",
    .qdev.size = sizeof(IntelIOMMUState),
    .qdev.vmsd = &vmstate_intel_iommu,
    .qdev.reset = intel_iommu_reset,
    .qdev.unplug	= intel_iommu_exit,
    .qdev.no_user = 1,
};

static void intel_iommu_register_devices(void)
{
	sysbus_register_withprop(&intel_iommu_info);
}

device_init(intel_iommu_register_devices)

